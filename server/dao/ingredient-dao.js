class IngredientDao {
  constructor() {}

  async createIngredient(ingredient) {}

  async getIngredient(id) {}

  async updateIngredient(ingredient) {}

  async deleteIngredient(id) {}

  async listIngredients() {}
}

module.exports = IngredientDao;
