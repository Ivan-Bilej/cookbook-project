class RecipeDao {
  constructor() {}

  async createRecipe(recipe) {}

  async getRecipe(id) {}

  async updateRecipe(recipe) {}

  async deleteRecipe(id) {}

  async listRecipes() {}
}

module.exports = RecipeDao;
